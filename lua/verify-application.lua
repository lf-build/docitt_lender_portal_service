local jwt = require "nginx-jwt"
local cjson = require "cjson"
local mlcache = require "resty.mlcache"
local M = {}

local cache, err = mlcache.new("my_cache", "cache_dict", {
   lru_size = 1000, -- hold up to 1000 items in the L1 cache (Lua VM)
   ttl      = 3600, -- caches scalar types and tables for 1h
   neg_ttl  = 0.1    -- caches nil values for 60s
})
if not cache then
    error("could not create mlcache: " .. err)
end


local function contains(list, x)
    for _, v in pairs(list) do
		if v == x then return true end
	end
	return false
end

local function isValidApplication(applicationNumber,auth_header)
    ngx.log(ngx.WARN, "Not found in cache, verifying for application ".. applicationNumber)
    jwt.auth();
    ngx.req.set_header("Authorization", auth_header)
    ngx.req.set_header("accept-encoding","")
    ngx.req.set_header("Content-Type", "application/json")
    local res = ngx.location.capture("/verify-application/"..applicationNumber,  {method = ngx.HTTP_GET})
    if res.status ~= 204 then
        return auth_header, false
    end
    return applicationNumber
end


function M.verifyApplicationNumber(applicationNumber)
    local auth_header = ngx.var.http_Authorization
    local indexOfSlash=string.find(applicationNumber, '/', 1, true);
    if indexOfSlash~= nil then
        applicationNumber=string.sub(applicationNumber,0,indexOfSlash-1);
    end 
    local value, err = cache:get(auth_header, nil ,isValidApplication, applicationNumber ,auth_header);
    if err then
        ngx.exit(ngx.HTTP_UNAUTHORIZED)
        return
    end
    if value~=nil then
        if value==applicationNumber then
            return
        end
    end
    ngx.exit(ngx.HTTP_UNAUTHORIZED)
end

return M;